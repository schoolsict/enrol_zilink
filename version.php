<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version    = 2014052000;
$plugin->requires   = 2013051400.00;
$plugin->component  = 'enrol_zilink';
$plugin->maturity   = MATURITY_STABLE;
$plugin->release    = 'v2.0.1 (Build: '.$plugin->version.')';
$plugin->cron       = 300;

$plugin->dependencies = array(
    'local_adminer'             => ANY_VERSION,
    'block_progress'            => ANY_VERSION,
    'local_zilink'              => ANY_VERSION,
    'auth_zilink_guardian'      => ANY_VERSION,
    'enrol_zilink_cohort'       => ANY_VERSION,
    'enrol_zilink_guardian'     => ANY_VERSION,
);